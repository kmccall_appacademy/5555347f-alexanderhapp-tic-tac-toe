class Board
  attr_reader :grid, :left_diagonal, :right_diagonal

  LEFT_DIAGONAL_POSITIONS = [[0, 0], [1, 1], [2, 2]].freeze
  RIGHT_DIAGONAL_POSITIONS = [[0, 2], [1, 1], [2, 0]].freeze

  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def left_diagonal
    @left_diagonal = [self[[0, 0]], self[[1, 1]], self[[2, 2]]]
  end

  def right_diagonal
    @right_diagonal = [self[[0, 2]], self[[1, 1]], self[[2, 0]]]
  end

  def [](position)
    row, col = position
    @grid[row][col]
  end

  def []=(position, mark)
    row, col = position
    @grid[row][col] = mark
  end

  def place_mark(position, mark)
    self[position] = mark if empty?(position)
  end

  def empty?(position)
    self[position].nil?
  end

  def winning_move
    each_with_row_col_pos do |mark, row, col, pos, l_diagonal, r_diagonal|
      if has_winning_move?(row) ||
         has_winning_move?(col) ||
         has_winning_move?(l_diagonal) ||
         has_winning_move?(r_diagonal)
        return pos if mark.nil?
      end
    end
    nil
  end

  def winner
    each_with_row_col_pos do |mark, row, col, pos, l_diagonal, r_diagonal|
      if is_winner?(row, mark) ||
         is_winner?(col, mark) ||
         is_winner?(l_diagonal, mark) ||
         is_winner?(r_diagonal, mark)
        return mark
      end
    end
    nil
  end

  def tie?
    grid_filled? && winner.nil?
  end

  def over?
    tie? || !winner.nil? ? (return true) : (return false)
  end

  def display
    print "\n    " + "0".ljust(4) + "1".ljust(4) + "2".ljust(4)
    print "\n  -------------\n"
    grid.each_with_index do |row, idx|
      print idx.to_s.ljust(2) + "|".ljust(2)
      row.each { |el| print el.to_s.ljust(2) + "|".ljust(2) }
      puts "\n  -------------"
    end
  end

  private

  def each_with_row_col_pos(&blk)
    grid.each_with_index do |row, row_idx|
      row.each_with_index do |el, col_idx|
        position = [row_idx, col_idx]
        l_diagonal, r_diagonal = [], []
        r_diagonal = right_diagonal if RIGHT_DIAGONAL_POSITIONS.include?(position)
        l_diagonal = left_diagonal if LEFT_DIAGONAL_POSITIONS.include?(position)
        column = grid.transpose[col_idx]
        blk.call(el, row, column, position, l_diagonal, r_diagonal)
      end
    end
  end

  def is_winner?(row, mark)
    if mark.nil? || row.empty?
      false
    elsif row.all? { |el| el == mark }
      true
    else
      false
    end
  end

  def has_winning_move?(row)
    %i(X O).each do |sym|
      return true if row.count(sym) == 2 && row.count(nil) == 1
    end
    false
  end

  def grid_filled?
    grid.each do |row|
      row.each { |el| (return false) if el.nil? }
    end
    true
  end

end
