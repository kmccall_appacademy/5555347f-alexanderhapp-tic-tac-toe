require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :current_player

  def initialize(player_one, player_two)
    @board = Board.new
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
  end

  def play
    player_one.mark = :X
    player_two.mark = :O

    board.display
    until board.over?
      play_turn
      current_player.display(board)
    end

    switch_players!
    if board.tie?
      puts "\nIt's a tie!"
    else
      puts "\nCongatulations, #{current_player.name}! You win!"
    end
    board.display
  end

  def play_turn
    move = current_player.get_move
    mark = current_player.mark
    board.place_mark(move, mark).nil? ? (return nil) : board.place_mark(move, mark)
    switch_players!
  end

  def switch_players!
    current_player == player_one ? @current_player = player_two : @current_player = player_one
  end

  if __FILE__ == $PROGRAM_NAME
    alex = HumanPlayer.new("Alex")
    edith = ComputerPlayer.new

    game = Game.new(alex, edith)

    game.play

  end
end
