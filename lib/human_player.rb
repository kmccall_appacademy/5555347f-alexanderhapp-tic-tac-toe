class HumanPlayer
  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
  end

  def mark=(mark)
    @mark = mark
  end

  def get_move
    puts "\n#{name}, where would you like to move? (row, column)"
    gets.chomp.split(",").map(&:to_i)
  end

  def display(board)
    board.display
  end
end
