class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name="Edith")
    @name = name
  end

  def mark=(mark)
    @mark = mark
  end

  def display(board)
    @board = board
  end

  def get_move
    board.winning_move || [rand(3), rand(3)]
  end

end
